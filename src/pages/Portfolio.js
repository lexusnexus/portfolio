import React from 'react';
import HeaderPortfolio from './../components/HeaderPortfolio';
import PortfolioContents from './../components/PortfolioContents';
import Footer from './../components/Footer';
import {Container} from 'react-bootstrap';
export default function Portfolio() {
	return (
		<Container fluid className="p-0 m-0">
			<HeaderPortfolio/>
			<PortfolioContents/>
			<Footer/>
		</Container>
		)
}