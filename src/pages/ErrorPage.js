import React from "react";

export default function ErrorPage() {
   return (
      <div id="errorPage">
              <div class="text-center">
                  <p>404 | Page not found</p>
                  <p class="message">Sorry, the page that you are looking for is not available</p>
              </div>
          </div>
   );
}
