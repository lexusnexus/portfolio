import React from 'react';
import HeaderContact from './../components/HeaderContact';
import Contact from './../components/Contact';
import Footer from './../components/Footer';
import {Container} from 'react-bootstrap';
export default function ContactPage() {
	return (
		<Container fluid className="p-0 m-0">
			<HeaderContact/>
			<Contact/>
			<Footer/>
		</Container>
		)
}