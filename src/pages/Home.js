import React from 'react';
import Banner from './../components/Banner';
import About from './../components/About';
import Works from './../components/Works';
import ToPortfolio from './../components/ToPortfolio';
import Contact from './../components/Contact';
import Footer from './../components/Footer';
import {Container} from 'react-bootstrap';
export default function Home() {
	return (
		<Container fluid className="p-0 m-0">
			<Banner/>
			<About/>
			<Works/>
			<ToPortfolio/>
			<Contact/>
			<Footer/>
		</Container>
		)
}