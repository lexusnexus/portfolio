import AOS from "aos";
import "aos/dist/aos.css";
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Portfolio from "./pages/Portfolio";
import ContactPage from "./pages/ContactPage";
import ErrorPage from "./pages/ErrorPage";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

export default function App() {
   useEffect(() => {
      AOS.init();
   });
   useEffect(() => {
      switch (window.location.pathname) {
         case "/":
            document.title = "Melai The Coder";
            break;
         case "/portfolio":
            document.title = "Portfolio | Melai The Coder";
            break;
         case "/contact":
            document.title = "Contact | Melai The Coder";
            break;
         default:
            document.title = "Page not found | Melai The Coder";
            break;
      }
   }, [window.location.pathname]);
   return (
      <Router>
         <AppNavbar />
         <Routes>
            <Route path="/" element={<Home />} />
             <Route path="/portfolio" element={<Portfolio />} />
              <Route path="/contact" element={<ContactPage />} />
              <Route path="*" element={<ErrorPage />} />
         </Routes>
      </Router>
   );
}
