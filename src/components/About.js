import React from "react";
import melaiPic from "./../images/melai-new.png";
export default function About() {
   return (
      <section id="about" class="py-5">
         <div class="primary-accent"></div>
         <div class="container aboutContainer pt-5">
            <div class="row">
               <div class="col-12 col-md-4 offset-md-7">
                  <h2>About</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-6 mb-3" data-aos-duration="1500" data-aos="zoom-in-right">
                  <figure>
                     <img src={melaiPic} alt="Melanie Dotollo" />
                  </figure>
               </div>
               <div class="col-12 col-md-5 offset-md-1" data-aos-duration="1500" data-aos="zoom-in-down">
                  <h3>Melanie Dotollo</h3>
                  <p>
                  "Curiosity is the wick in the candle of knowledge."

               

                  </p>
                  <p>
                   Long ago, I was curious about how websites work on the internet. I started wondering what was happening when I clicked a button. Curiosity leads me to knowledge, but there is a bridge between my knowledge and my curiosity, and that is passion. My curiosity came out as the wick of my candle of knowledge, lit up by my passion. Since then, I've been designing and building well-designed and functional websites, and I love what I do. I've studied various programming languages, including MERN Stack, HTML, CSS, Javascript, PHP, MySQL, Laravel, Basic SEO, and more. I've also worked on testing tools such as Pentaho, Soap UI, and Postman. 
                  </p>
               </div>
            </div>
         </div>
      </section>
   );
}
