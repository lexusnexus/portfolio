import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import Swal from 'sweetalert2'
export default function ContactUs() {
   function sendEmail(e) {
      e.preventDefault();

      emailjs.sendForm("service_kapvvgq", "template_ldnlobv", e.target, "HCAF7_V0N2ebCeUGI").then(
         (result) => {
            Swal.fire(
              'Message Sent!',
              'Your message has been sent successfully.',
              'success'
            )
         },
         (error) => {
            console.log(error.text);
         }
      );
    
      e.target.reset();
   }

   return (
      <div id="contact" className="pt-3">
         <div className="container py-5">
            <div class="row">
               <div class="col-12 col-md-10 col-lg-8 mx-auto">
                  <h2>Contact</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-12 col-md-8 col-lg-6 mx-auto">
                  <h3>Have question? Please message me directly</h3>
                  <form onSubmit={sendEmail}>
                     <div className="row pt-3 mx-auto">
                        <div className="form-group">
                           <input type="text" required className="form-control" placeholder="Name" name="name" />
                        </div>
                        <div className="form-group pt-2">
                           <input type="email" required className="form-control" placeholder="Email Address" name="email" />
                        </div>
                        <div className="form-group pt-2">
                           <input type="text" required className="form-control" placeholder="Subject" name="subject" />
                        </div>
                        <div className="form-group pt-2">
                           <textarea required className="form-control" id="" cols="30" rows="8" placeholder="Your message" name="message"></textarea>
                        </div>
                        <div className="pt-3">
                           <input type="submit" className="btn btn-primary toWhite" value="Send Message"></input>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   );
}
