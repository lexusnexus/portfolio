import React,{useState} from "react";
import { Nav, Navbar, Container } from "react-bootstrap";
import { NavLink } from "react-router-dom";
export default function AppNavbar() {
    const [navbar, setNavbar] = useState(false);
    const changeBackground = () => {
      if (window.scrollY >= 20) {
         setNavbar(true);
      } else {
         setNavbar(false);
      }
   };
   window.addEventListener("scroll", changeBackground);
   return (
      <Navbar collapseOnSelect expand="lg" variant={navbar ? "dark" : "light"} fixed="top" className={navbar ? "navbarMove" : "navbarHead"}>
         <Container>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
               <Nav className="me-auto">
                  <Nav.Link href="/" className={window.location.pathname === "/" ? "active" : null}>
                     About
                  </Nav.Link>
                  <Nav.Link href="/portfolio"  className={
                    ( window.location.pathname === "/portfolio" ||
                     window.location.pathname === "/portfolio#pickmeID" ||
                     window.location.pathname=== "/portfolio#gmailID" )
                     ? "active" : null}>Portfolio</Nav.Link>
                  <Nav.Link href="/contact" className={window.location.pathname === "/contact" ? "active" : null}>Contact</Nav.Link>
                  
               </Nav>
               <Nav className="d-none d-lg-block">
                  <a href="https://www.linkedin.com/in/melaithecoder/" target="_blank" className={navbar ? "social-in px-3 navbarLight" : "social-in px-3 navbarIcon"}  rel="noopener noreferrer">
                     <i class="fab fa-linkedin-in"></i>
                  </a>
                  <a href="https://www.facebook.com/Melai.Dotollo" target="_blank" className={navbar ? "social-fb px-3 navbarLight" : "social-fb px-3 navbarIcon"}  rel="noopener noreferrer">
                     <i class="fab fa-facebook-square"></i>
                  </a>
               </Nav>
            </Navbar.Collapse>
         </Container>
      </Navbar>
   );
}
