import React from "react";
import airgo1 from "./../images/airgo1.png";
import airgo2 from "./../images/airgo2.png";
import pickme1 from "./../images/pickme1.png";
import pickme2 from "./../images/pickme2.png";
import gmail1 from "./../images/gmail1.png";
import gmail2 from "./../images/gmail2.png";
import melai1 from "./../images/melai1.png";
import melai2 from "./../images/melai2.png";
import owl1 from "./../images/owlsecrets-freedomwall-online-inbox.png";
import owl2 from "./../images/owlsecrets-freedomwall-online-about.png";


export default function PortfolioContents() {
   return (
      <section id="portfolio-page">
         <div class="portfolio-cont" id="owlID">
            <div class="container">
               <div class="row portfolio-row">
                  <div
                     class="col-12 col-md-6 col-lg-5 text-md-right order-2
                           offset-lg-0"
                  >
                     <div class="portfolio-entry-container">
                        <div class="portfolio-entry-image">
                           <img src={owl1} alt="" />
                        </div>
                        <div class="portfolio-entry-image">
                           <img src={owl2} alt="" />
                        </div>
                     </div>
                  </div>
                  <div
                     class="col-12 col-md-6 col-lg-5 order-1
                           offset-lg-1"
                  >
                     <dl>
                        
                        <dd class="title">
                                   <a href="https://owlsecrets-freedomwall.online/" target="_blank">Owl Secrets | Online Freedom Wall</a>
                                </dd>
                       
                        <dd>It is an <strong>online freedom wall</strong>  where we can anonymously say anything to our loved ones. This website aims to provide an emotional outlet for those in need.
                         
                                All messages are submitted and stored digitally in an online storage.
                                <br/>
                                <br/>
                                This entry showcases my technical skills in <strong>Laravel, PHP, MySQL,</strong> and <strong>Basic SEO</strong>. It has a <strong>responsive design</strong> which means it is compatible with any device. It uses <strong>SweetAlert</strong> to notify the users of a successful posting of the message. It uses <strong>Git</strong> for the version control system and stores the Git repository in GitLab.
                                     <br/>
                                     <br/>
                                    The <strong>domain</strong> is bought and registered at Hostinger with <strong>SSL Certificate</strong>. It stores the database and hosts the webpage at Hostinger. It has <strong>robots.txt</strong> that allows what crawlers can crawl the site, disallow blocking the crawlers on specific pages, and a sitemap of the website. The website is <strong>indexed</strong> based on <strong>Google Search Console</strong>.
                                    <br/>
                                    <br/>
                                    Using <strong>SEMrush</strong>, a popular SEO tool, to site audit the domain. SEMrush has features that look for duplicate content and canonical issues on the site and produce an easy-to-understand report to fix those issues. The site health is <strong>96%</strong> with no errors. 

                                </dd>
                        <a href="https://owlsecrets-freedomwall.online/" class="btn btn-primary demoLink toWhite" target="_blank">
                           View Demo Link <i class="fas fa-arrow-right"></i>
                        </a>
                     </dl>
                  </div>
               </div>
            </div>
         </div> 
         <div class="portfolio-cont" id="pickmeID">
            <div class="container">
               <div class="row portfolio-row">
                  <div
                     class="col-12 col-md-6 col-lg-5 text-md-right order-2
                                order-md-1 offset-lg-1"
                  >
                     <div class="portfolio-entry-container">
                        <div class="portfolio-entry-image">
                           <img src={pickme1} alt="" />
                        </div>
                        <div class="portfolio-entry-image">
                           <img src={pickme2} alt="" />
                        </div>
                     </div>
                  </div>
                  <div
                     class="col-12 col-md-6 col-lg-5 order-1
                                    order-md-2"
                  >
                     <dl>
                        
                        <dd class="title">
                           <a href="https://pickmeshop.vercel.app/" target="_blank">Pick Me Retailers and Resellers Shop</a>
                        </dd>
                       
                        <dd>
                       An <strong>e-commerce website</strong> used by the customers and admins of Pick Me Retailers and Resellers Shop can process orders, accept payments, manage shipping and logistics, and provide customer service. Some features are order management, product management, registration and login, product search, etc. It is a <strong>responsive design</strong> compatible with any device to bring convenience to the customers and admins. 
                        <br/>
                         <br/>
                          It is a project made using MERN. This entry showcases my technical skills in using the <strong>famous MERN stack</strong>, customizing Bootstrap using React Bootstrap, using different React Packages and React Hooks, database design, and REST API. MongoDB as the database and Cloudinary as the storage for the images. The back-end of this application is hosted on Heroku, while the front-end is on Vercel. It uses <strong>Git</strong> for the version control system and stores the Git repository in GitLab.
                        </dd>
                        <a href="https://pickmeshop.vercel.app/" class="btn btn-primary demoLink toWhite" target="_blank">
                           View Demo Link <i class="fas fa-arrow-right"></i>
                        </a>
                     </dl>
                  </div>
               </div>
            </div>
         </div>
         <div class="portfolio-cont" id="gmailID">
            <div class="container">
               <div class="row portfolio-row">
                  <div
                     class="col-12 col-md-6 col-lg-5 text-md-right order-2
                                offset-lg-0"
                  >
                     <div class="portfolio-entry-container">
                        <div class="portfolio-entry-image">
                           <img src={gmail1} alt="" />
                        </div>
                        <div class="portfolio-entry-image">
                           <img src={gmail2} alt="" />
                        </div>
                     </div>
                  </div>
                  <div
                     class="col-12 col-md-6 col-lg-5 order-1
                                    offset-lg-1"
                  >
                     <dl>
                        
                        <dd class="title">
                           <a href="https://gmailapp.vercel.app/" target="_blank">Gmail Clone</a>
                        </dd>
                       
                        <dd>It is a clone of Gmail that can access using a <strong>Google Account</strong>. It consists of 100 emails from your Gmail. We can view the emails individually.
                        <br/>
                            It uses <strong>Google APIs</strong> for Google Login and Google Mail. It also has the <strong>same loader of GMail</strong> while loading. It has a <strong>responsive design</strong> which means it is compatible with any device.
                            <br/>
                           We use <strong>Git</strong> for the version control system and store the Git repository in GitLab.</dd>
                        <a href="https://gmailapp.vercel.app/" class="btn btn-primary demoLink toWhite" target="_blank">
                           View Demo Link <i class="fas fa-arrow-right"></i>
                        </a>
                     </dl>
                  </div>
               </div>
            </div>
         </div>
         <div class="portfolio-cont" id="airgoID">
            <div class="container">
               <div class="row portfolio-row">
                  <div
                     class="col-12 col-md-6 col-lg-5 text-md-right order-2
                                order-md-1 offset-lg-1"
                  >
                     <div class="portfolio-entry-container">
                        <div class="portfolio-entry-image">
                           <img src={airgo1} alt="" />
                        </div>
                        <div class="portfolio-entry-image">
                           <img src={airgo2} alt="" />
                        </div>
                     </div>
                  </div>
                  <div
                     class="col-12 col-md-6 col-lg-5 order-1
                                    order-md-2"
                  >
                     <dl>
                        
                        <dd class="title">
                           <a href="https://airgonomicservices.vercel.app/" target="_blank">Airgonomic Airconditioning Services</a>
                        </dd>
                       
                        <dd>It is a <strong>company portfolio website</strong> of Airgonomic Airconditioning Services. It has a <strong>responsive design</strong> which means it is compatible with any device. It uses <strong>Animation On Scroll (AOS)</strong> implemented in React to animate elements. It uses <strong>Git</strong> for the version control system and stores the Git repository in GitLab.</dd>
                        <a href="https://airgonomicservices.vercel.app/" class="btn btn-primary demoLink toWhite" target="_blank">
                           View Demo Link <i class="fas fa-arrow-right"></i>
                        </a>
                     </dl>
                  </div>
               </div>
            </div>
         </div>
        <div class="portfolio-cont" id="melaiID">
                    <div class="container">
                       <div class="row portfolio-row">
                          <div
                             class="col-12 col-md-6 col-lg-5 text-md-right order-2
                                        offset-lg-0"
                          >
                             <div class="portfolio-entry-container">
                                <div class="portfolio-entry-image">
                                   <img src={melai1} alt="" />
                                </div>
                                <div class="portfolio-entry-image">
                                   <img src={melai2} alt="" />
                                </div>
                             </div>
                          </div>
                          <div
                             class="col-12 col-md-6 col-lg-5 order-1
                                            offset-lg-1"
                          >
                             <dl>
                                
                                <dd class="title">
                                   <a href="https://melaithecoder.vercel.app/" target="_blank">Melai The Coder</a>
                                </dd>
                               
                                <dd>My <strong>e-portfolio</strong>  is an online representation of the works I have created and my skills and experiences. It has a <strong>responsive design</strong> which means it is compatible with any device. The contact component uses <strong>EmailJS</strong> to send an email directly to me, while it uses <strong>SweetAlert</strong> to alert a successful process of sending a message to me. It uses <strong>Animation On Scroll (AOS)</strong> implemented in React to animate elements. It uses <strong>Git</strong> for the version control system and stores the Git repository in GitLab.</dd>
                                <a href="https://melaithecoder.vercel.app/" class="btn btn-primary demoLink toWhite" target="_blank">
                                   View Demo Link <i class="fas fa-arrow-right"></i>
                                </a>
                             </dl>
                          </div>
                       </div>
                    </div>
         </div>
      </section>
   );
}
