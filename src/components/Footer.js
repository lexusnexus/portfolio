import React from "react";
import melaiPic from "./../images/melai.png";
export default function Footer() {
   return (
      <div id="footer">
         <div class="container">
            <div class="row">
               <div class="col-12 text-center">
                  <p>Melanie Dotollo</p>
                  <div id="social-icons">
                     <a href="https://www.linkedin.com/in/melaithecoder/" target="_blank" class="social-icons" rel="noopener noreferrer">
                        <i class="fab fa-linkedin-in"></i>
                     </a>
                     <a href="https://www.facebook.com/Melai.Dotollo" target="_blank" class="social-icons" rel="noopener noreferrer">
                        <i class="fab fa-facebook-square"></i>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   );
}
