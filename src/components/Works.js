import React from "react";
import { HashLink } from 'react-router-hash-link';
import pickme1 from "./../images/pickme1.png";
import pickme2 from "./../images/pickme2.png";
import gmail1 from "./../images/gmail1.png";
import gmail2 from "./../images/gmail2.png";
export default function Works() {
   return (
      <section id="portfolio">
         <div class="portfolio-header pt-5">
            <div class="container">
               <div class="row">
                  <div class="col-12 col-md-10 ">
                     <h2>Portfolio</h2>
                  </div>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row" id="portfolio-entries">
               <div class="entry col-md-5 ">
                  <div class="portfolio-entry" data-aos="fade-up" data-aos-duration="1000">
                     <div class="row">
                        <div class="col-md-10">
                           <h3>Pick Me Retailers and Resellers Shop</h3>
                           <p>An <strong>e-commerce website</strong> used by the customers and admins of Pick Me Retailers and Resellers Shop can process orders, accept payments, manage shipping and logistics, and provide customer service. Some features are order management...</p>
                           <HashLink smooth to="/portfolio#pickmeID" class="btn btn-outline-primary demo-link">
                            Read More <i class="fas fa-arrow-right"></i>
                           </HashLink>
                           <div id="screenshot" class="d-flex screenshots">
                              <figure>
                                 <img src={pickme1} alt="" />
                              </figure>

                              <figure>
                                 <img src={pickme2} alt="" />
                              </figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="entry col-md-5 offset-md-2">
                  <div class="portfolio-entry" data-aos="fade-up" data-aos-duration="1000">
                     <div class="row">
                        <div class="col-md-10">
                           <h3>Gmail Clone</h3>
                           <p>It is a clone of Gmail that can access using a <strong>Google Account</strong>. It consists of 100 emails from your Gmail. We can view the emails individually.

                            It uses <strong>Google APIs</strong> for Google Login and Google Mail. It also has the <strong>same loader of GMail</strong> while loading...</p>
                           <HashLink smooth to="/portfolio#gmailID" class="btn btn-outline-light demo-link">
                            Read More <i class="fas fa-arrow-right"></i>
                           </HashLink>
                           <div id="screenshot" class="d-flex screenshots">
                              <figure>
                                 <img src={gmail1} alt="" />
                              </figure>

                              <figure>
                                 <img src={gmail2} alt="" />
                              </figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   );
}
