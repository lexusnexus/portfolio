import React from "react";
import airgo from "./../images/airgo.png";
import gmail from "./../images/gmailmeta.png";
import pickme from "./../images/meta.png";
import coder from "./../images/laptop-code-solid.png";
import { HashLink } from 'react-router-hash-link';
export default function ToPortfolio() {
   return (
     /*  <div class="container recentContainer p-5" id="recentID">
         <div class="row justify-content-md-center">
            <div class="col align-center mx-0">
               <h2 class="text-center text-blue" data-aos="fade-up"  data-aos-duration="900">My Recent Work</h2>
               <p class="text-center paraPort" data-aos="fade-up"  data-aos-duration="900">Here are a few design projects I've worked on recently. Want to know more? <a href="#contact">
                  Learn More
                  </a>.</p>
            </div>
         </div>

         <div class="row justify-content-md-center types">
            <div class="col-md-3 col-12 align-center recentDiv portfolio-entry-image mb-5">
               <img src={pickme} class="img-fluid lazyloaded recentImg" alt="pickme" />
                <HashLink smooth to="/portfolio#pickmeID"><div className="middle">
                  <div className="recentText">Pick Me Retailers and Resellers Shop</div>
               </div> </HashLink>
            </div>

            <div class="col-md-3 col-12 align-center recentDiv portfolio-entry-image mb-5">
               <img src={gmail} class="img-fluid lazyloaded recentImg" alt="gmail" />
               <HashLink smooth to="/portfolio#gmailID"><div className="middle">
                  <div className="recentText">Gmail Clone</div>
               </div> </HashLink>
            </div>

            <div class="col-md-3 col-12 align-center recentDiv portfolio-entry-image mb-5">
               <img src={airgo} class="img-fluid lazyloaded recentImg" alt="airgo" />
               <HashLink smooth to="/portfolio#airgoID"><div className="middle">
                  <div className="recentText">Airgonomic Airconditioning Services</div>
               </div> </HashLink>
            </div>

            <div class="col-md-3 col-12 align-center recentDiv portfolio-entry-image mb-5">
               <img src={coder} class="img-fluid lazyloaded recentImg" alt="coder" />
               <HashLink smooth to="/portfolio#melaiID"><div className="middle">
                  <div className="recentText">Melai The Coder</div>
               </div> </HashLink>
            </div>

         </div>
      </div>
   */
   <section id="link-to-portfolio" class="py-5 bg-lightgray">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <p>Want to see more?</p>
                  <a href="/portfolio" class="btn btn-primary toWhite">
                     View more sample works
                  </a>
               </div>
            </div>
         </div>
      </section>
 )}
