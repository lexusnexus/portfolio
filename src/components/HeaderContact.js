import React from "react";

export default function HeaderContact() {
   return (
      <header id="page-section-header">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <h1>Leave A Message</h1>
                  </div>
              </div>
          </div>
      </header>
   );
}
