import React from "react";

export default function Banner() {
   return (
      <section id="landing" class="py-5">
         <div class="container">
            <div class="row">
               <div class="col-12 col-md-10 col-lg-8 mx-auto" data-aos-duration="1500" data-aos="fade-up">
                  <h1>Hello! I'm Melai.</h1>
                  <p class="title">Full-Stack Web Developer</p>
                  <p>Let's create something awesome and fun.</p>
                  <a href="#about" class="btn-primary toWhite btn">
                     Know Me
                  </a>
               </div>
            </div>
         </div>
      </section>
   );
}
